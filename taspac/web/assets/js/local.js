$(document).ready(function(){
    
    /*$(".country_selector").countrySelect({
	  onlyCountries: ['us','cn']
	});*/
	
	$('.navbar-toggler').click(function(){
        
        if ($(this).hasClass('change')){
            $(this).removeClass('change');
        } else {
            $(this).addClass('change');
        };
    });
    
    $('.menu-list .plus-icon').click(function(){
        $(this).hide();
        $(this).siblings('.back-btn').show();
        $(this).parent('.menu-list').next().css('display','block');
        $(this).parent('.menu-list').css('border-bottom','2px solid #004159');
        $(this).parent('.menu-list').css('margin-bottom','10px');
        $(this).parent('.menu-list').css('padding-bottom','10px');
    });
    
    $('.menu-list .back-btn').click(function(){
        $(this).hide();
        $(this).siblings('.plus-icon').show();
        $(this).parent('.menu-list').next().css('display','none');
        $(this).parent('.menu-list').css('border-bottom','none');
        $(this).parent('.menu-list').css('margin-bottom','0px');
        $(this).parent('.menu-list').css('padding-bottom','0px');
    });

});

/*$(window).scroll(function() {
    if ($(this).scrollTop() > 70) {
        $('.site-header').addClass('sticky');
    } else {
        $('.site-header').removeClass('sticky');
    }
});*/




